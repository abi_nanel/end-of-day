package nanel.abi.endofday.model;

import java.math.BigDecimal;

import com.opencsv.bean.CsvBindByName;

import lombok.Data;

/**
 * @author Abi Nanel
 *
 */
@Data
public class EndOfDay {
	
	private Long id;
	private String name;
	private Integer age;
	private BigDecimal balanced;
	private BigDecimal prevBalanced;
	private BigDecimal avgBalanced;
	private Integer freeTransfer;
	
	private String threadName1;
	private String threadName2A;
	private String threadName2B;
	private String threadName3;
}
