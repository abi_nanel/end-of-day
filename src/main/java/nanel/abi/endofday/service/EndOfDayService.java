package nanel.abi.endofday.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

import nanel.abi.endofday.model.EndOfDay;

/**
 * @author Abi Nanel
 *
 */
@Service
public class EndOfDayService {
	
	Logger log = LoggerFactory.getLogger(EndOfDayService.class);
	
	//@Value("${file.path.output}")
    private String outputFile;
	
	//@Value("${file.path.input}")
    private String inputFile;
	
	@Async
	public CompletableFuture<EndOfDay> countAvgBalance(EndOfDay eod) {
		//log.info("count avg id ("+eod.getId()+") : "+Thread.currentThread().getName());
		BigDecimal avgBalanced = eod.getBalanced().add(eod.getPrevBalanced())
				.divide(new BigDecimal(2)).setScale(0, RoundingMode.HALF_UP);
		eod.setAvgBalanced(avgBalanced);
		eod.setThreadName1(Thread.currentThread().getName());
		
		return CompletableFuture.completedFuture(eod);
	}
	
	@Async
	public CompletableFuture<EndOfDay> applyBenefitA(EndOfDay eod) {
		//log.info("apply benefit A id ("+eod.getId()+") : "+Thread.currentThread().getName());
		if(eod.getBalanced().compareTo(new BigDecimal(100)) >= 0 
				&& eod.getBalanced().compareTo(new BigDecimal(150)) <= 0) {
			eod.setFreeTransfer(5);
		}
		eod.setThreadName2A(Thread.currentThread().getName());
		
		return CompletableFuture.completedFuture(eod);
	}
	
	@Async
	public CompletableFuture<EndOfDay> applyBenefitB(EndOfDay eod) {
		//log.info("apply benefit B id ("+eod.getId()+") : "+Thread.currentThread().getName());
		if(eod.getBalanced().compareTo(new BigDecimal(150)) > 0) {
			eod.setBalanced(eod.getBalanced().add(new BigDecimal(25)));
		}
		eod.setThreadName2B(Thread.currentThread().getName());
		
		return CompletableFuture.completedFuture(eod);
	}
	
	@Async("customExecutor")
	public CompletableFuture<EndOfDay> applyBonus(EndOfDay eod) {
		//log.info("apply bonus id ("+eod.getId()+") : "+Thread.currentThread().getName());
		if(eod.getId() <= 100L) {
			eod.setBalanced(eod.getBalanced().add(new BigDecimal(10)));
		}
		eod.setThreadName3(Thread.currentThread().getName());
		
		return CompletableFuture.completedFuture(eod);
	}
	
	public List<EndOfDay> parseCsvFile(MultipartFile file) throws Exception {
		log.info("parse csv file");
		List<EndOfDay> endOfDayList = new ArrayList<>();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
			String rowLine;
			int title = 0;
			while ((rowLine=br.readLine()) != null) {
				//skip header title
				if(title == 0) {
					title++;
					continue;
				}
					
				String[] data = rowLine.split(";");
				EndOfDay eod = new EndOfDay();
				eod.setId(Long.parseLong(data[0]));
				eod.setName(data[1]);
				eod.setAge(Integer.parseInt(data[2]));
				eod.setBalanced(new BigDecimal(data[3]));
				eod.setPrevBalanced(new BigDecimal(data[4]));
				eod.setAvgBalanced(new BigDecimal(data[5]));
				eod.setFreeTransfer(Integer.parseInt(data[6]));
				endOfDayList.add(eod);
			}
			log.info("parse csv file Done");
		} catch (Exception e) {
			log.info("parse csv file Failed {}", e);
			throw new Exception("error when parsing csv file {}", e);
		}
		
		return endOfDayList;
	}
	
	public void writeToCsv(List<EndOfDay> eodList) throws IOException {
		log.info("write list to csv file");
		List<String[]> dataCsv = new ArrayList<>();
		String[] header = {"id","Nama","Age","Balanced","No 2b Thread-No","No 3 Thread-No","Previous Balanced", 
				"Average Balanced","No 1 Thread-No","Free Transfer","No 2a Thread-No"};
		String[] value;
		dataCsv.add(header);
		for(EndOfDay eod : eodList) {
			value = new String[header.length];
			value[0] = String.valueOf(eod.getId());
			value[1] = String.valueOf(eod.getName());
			value[2] = String.valueOf(eod.getAge());
			value[3] = eod.getBalanced().toPlainString();
			value[4] = eod.getThreadName2B();
			value[5] = eod.getThreadName3();
			value[6] = eod.getPrevBalanced().toPlainString();
			value[7] = eod.getAvgBalanced().toPlainString();
			value[8] = eod.getThreadName1();
			value[9] = String.valueOf(eod.getFreeTransfer());
			value[10] = eod.getThreadName2A();
			
			dataCsv.add(value);
		}
		
		try (ICSVWriter writer = new CSVWriterBuilder(
		          new FileWriter(outputFile))
		          .withSeparator(';')
		          .build()) {
		      writer.writeAll(dataCsv);
		  } catch (IOException e) {
			  log.info("error when write to csv file failed {}", e);
			  throw new IOException("error when parsing csv file {}", e);
		}
	}
	
	public List<EndOfDay> readFileAndConvertToCsv() throws Exception {
		log.info("find file csv and convert to object");
		List<EndOfDay> listEod = new ArrayList<>();
		EndOfDay eod = new EndOfDay();
		CSVParser csvParser = new CSVParserBuilder().withSeparator(';').build(); // custom separator
		try (CSVReader reader = new CSVReaderBuilder(new FileReader(inputFile))
				.withCSVParser(csvParser) // custom CSV parser
				.withSkipLines(1) // skip the first line, header info
				.build()) {
			for (String[] value : reader.readAll()) {
				eod = new EndOfDay();
				eod.setId(Long.parseLong(value[0]));
				eod.setName(value[1]);
				eod.setAge(Integer.parseInt(value[2]));
				eod.setBalanced(new BigDecimal(value[3]));
				eod.setPrevBalanced(new BigDecimal(value[4]));
				eod.setAvgBalanced(new BigDecimal(value[5]));
				eod.setFreeTransfer(Integer.parseInt(value[6]));

				listEod.add(eod);
			}
		}
		
		log.info("find file csv and convert to object Done");
		return listEod;
	}
	
	public void writeEODToCsv(List<EndOfDay> eodList, HttpServletResponse response) throws Exception {
		// set file name and content type
		String filename = "After Eod.csv";

		response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + filename + "\"");
	      
        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE);
        String[] csvHeader = {"id", "Nama", "Age", "Balanced", "No 2b Thread-No", "No 3 Thread-No", "Previous Balanced", 
        		"Average Balanced", "No 1 Thread-No", "Free Transfer", "No 2a Thread-No"};
        String[] nameMapping = {"id", "name", "age", "balanced", "threadName2B", "threadName3", "prevBalanced", 
        		"avgBalanced", "threadName1", "freeTransfer", "threadName2A"};
         
        csvWriter.writeHeader(csvHeader);
         
        for (EndOfDay eod : eodList) {
            csvWriter.write(eod, nameMapping);
        }
         
        csvWriter.close();
    }
}
