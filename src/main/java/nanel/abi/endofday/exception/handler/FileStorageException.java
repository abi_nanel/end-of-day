/**
 * 
 */
package nanel.abi.endofday.exception.handler;

/**
 * @author ASYST
 *
 */
public class FileStorageException extends RuntimeException {

	public FileStorageException(String message) {
        super(message);
    }

    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
