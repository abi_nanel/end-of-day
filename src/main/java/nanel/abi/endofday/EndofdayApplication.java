package nanel.abi.endofday;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import nanel.abi.endofday.config.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class EndofdayApplication {

	public static void main(String[] args) {
		SpringApplication.run(EndofdayApplication.class, args);
	}
}
