package nanel.abi.endofday.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import nanel.abi.endofday.model.EndOfDay;
import nanel.abi.endofday.model.UploadFileResponse;
import nanel.abi.endofday.service.EndOfDayService;
import nanel.abi.endofday.service.FileStorageService;

/**
 * @author Abi Nanel
 *
 */
@RestController
public class EndOfDayController {
	
//	static final String END_OF_DAY = "/eod";
//	static final String SUBMIT = END_OF_DAY+"/submit";
//	
//	Logger log = LoggerFactory.getLogger(EndOfDayController.class);
//	
//	@Value("${budget}")
//    private Long budget;
	
//	@Autowired
//	private EndOfDayService eodService;
	
	@Autowired
    private FileStorageService fileStorageService;
	
//	@SuppressWarnings("rawtypes")
//	@PostMapping(value = SUBMIT, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
//	public ResponseEntity processUpload(@RequestParam("file") MultipartFile file, HttpServletResponse res) throws Exception {
//		log.info("process Upload");
		/*List<EndOfDay> endOfDayList = eodService.parseCsvFile(file);
		if(endOfDayList.size() < 1) {
			Map<String, String> response = new HashMap<>();
			response.put("message", "file is empty");
			response.put("status", "failed");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		
		log.info("count average balance");
		CompletableFuture<EndOfDay> cfEod = new CompletableFuture<EndOfDay>();
		CompletableFuture<EndOfDay> cfEod2 = new CompletableFuture<EndOfDay>();
		for(EndOfDay eod : endOfDayList) {
			cfEod = eodService.countAvgBalance(eod);
		}
		// wait for all thread done to the next step
		CompletableFuture.allOf(cfEod).join();
		
		log.info("apply benefit A and B to nasabah");
		cfEod = new CompletableFuture<EndOfDay>();
		cfEod2 = new CompletableFuture<EndOfDay>();
		for(EndOfDay eod : endOfDayList) {
			cfEod = eodService.applyBenefitA(eod);
			cfEod2 = eodService.applyBenefitB(eod);
		}
		CompletableFuture.allOf(cfEod, cfEod2).join();
		
		log.info("apply bonus for first 100 row (id 1-100)");
		cfEod = new CompletableFuture<EndOfDay>();
		for(EndOfDay eod : endOfDayList) {
			cfEod = eodService.applyBonus(eod);
		}
		CompletableFuture.allOf(cfEod).join();
		
		//write list object to csv file then export
		eodService.writeEODToCsv(endOfDayList, res);
		
		log.info("process EOD Done.");*/
		
		
		
//		return new ResponseEntity<>(HttpStatus.OK);
//	}
	
    
    @PostMapping(value = "/uploadFile", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = fileStorageService.storeFile(file);
        
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
