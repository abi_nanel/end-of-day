package nanel.abi.endofday.config;

import java.util.concurrent.Executor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @author Abi Nanel
 *
 */
@Configuration
@EnableAsync
public class AsyncConfig {
	
	@Bean("taskExecutor")
	@Primary
	public Executor taskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(2);
		executor.setMaxPoolSize(2);
		executor.setQueueCapacity(400);
		executor.setThreadNamePrefix("Thread-");
		executor.initialize();
		
		return executor;	
	}
	
	@Bean("customExecutor")
	public Executor customExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(8);
		executor.setMaxPoolSize(8);
		executor.setQueueCapacity(200);
		executor.setThreadNamePrefix("Thread-");
		executor.initialize();
		
		return executor;	
	}
}
